# DeFiantHTS #



### What is DeFiantHTS? ###

**DeFiantHTS** is a decentralized financial ecosystem that implements **Hedera Token Service (HTS)**.  

**DeFiantHTS** consists of these components:

**DAGwood** is an HTS-based browser extension. 

**Hedera2Hiro (H2H)** is a wrapped **HBAR** & **STX** token and interoperability protocol 
between the **Hedera** and **Stacks 2.0** Testnets / Mainnets using **HTS** and **Clarity** smart contracts.     

**PyHTS** is a **Hedera** Python SDK.